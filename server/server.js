const express = require('express')
const cors= require('cors')
const ERouter= require('./routes/emp')

const app = express()

app.use(cors('*'))
app.use(express.json())
app.use(ERouter)


app.listen(4000,'0.0.0.0',()=>
{
    console.log('server started at 4000')
})