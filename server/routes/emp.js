const express = require('express')
const routes = express.Router()
const utils = require('../utils')

routes.get('/',(req,res)=>
{
   const statement =`select * from Emp`
   const conn= utils.getConnection()
    conn.query(statement,(error,data)=>
    {
      res.send(utils.getResult(error,data))
    })

})

routes.post('/',(req,res)=>
{
    const{name,salary,age}=req.body
   const statement =`insert into Emp(name,salary,age) values('${name}',${salary},${age});`
   const conn= utils.getConnection()
    conn.query(statement,(error,data)=>
    {
      res.send(utils.getResult(error,data))
    })

})

routes.put('/:id',(req,res)=>
{
    const{id}=req.params
    const{name,salary,age}=req.body
   const statement =`Update Emp set name='${name}',salary=${salary},age=${age} where empid = ${id}`
   const conn= utils.getConnection()
    conn.query(statement,(error,data)=>
    {
      res.send(utils.getResult(error,data))
    })

})
routes.delete('/:id',(req,res)=>
{
    const{id}=req.params
   const statement =`delete from Emp where empid=${id}`
   const conn= utils.getConnection()
    conn.query(statement,(error,data)=>
    {
      res.send(utils.getResult(error,data))
    })

})
module.exports=routes